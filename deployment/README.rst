Project Configuration
=====================

This folder contains configuration files for running the project as deployed
software, locally for development and on remote hosts.

`application/ <application/>`__
    Web server to Python interface configuration
`database/ <database/>`__
    Database service deployment configuration
`webserver/ <webserver/>`__
    Web server configuration for the project

See Also
--------

Application (framework) specific settings are located in the project's
``application`` module (see ``application/settings.py``).
